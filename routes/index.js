var express = require('express');
var router = express.Router();
var request = require("request");
/* GET home page. */

router.get('/pollution/:city', function(req, res, next) {
    var dateFormat = require('dateformat');
    var now = new Date();
    var url = "http://www2.prevair.org/ineris-web-services.php?url=atmo&date=" + dateFormat(now, "yyyymdd");

    request(url, function(err, resp, body) {
        if (err) {
            res.status(500)
            res.send(err);
            return;
        }
        body = JSON.parse(body);
        var isOk = false;
        var valeur = 0;
        for(var i in body) {
            if (body[i][4] == req.params.city.toUpperCase()){
                isOk = true;
                valeur = i;
            }
        }
        if (isOk){
            res.respond(body[valeur][11], 200);
            /*  Juste avec express sans l'objet réponse
            res.status(200)
            res.send(body[i][11]);*/
        } else {
            res.respond("La ville n'a pas été trouvée", 404);
            /* Juste avec express sans l'objet réponse
            res.status(404);
            res.send("La ville n'a pas été trouvée");*/
            //res.render('index', { title: "Pollution", resp: "La ville n'a pas été trouvée"});
        }
    });
    //http://www2.prevair.org/ineris-web-services.php?url=atmo&date=20171002
});

router.get('/pollution/api/docs', function(req, res, next) {
    res.sendfile('doc.html');
});

module.exports = router;
